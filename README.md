# **Równoległy algorytm genetyczny**  - Seating Assignment Problem *(temat 27)*

## Cel projektu

Celem projektu było opracowanie rozwiązania przy użyciu algorytmu genetycznego wybranego problemu optymalizacji. 

# Wybrany problem optymalizacji

Zaimplementowano program rozwiązujący Seating Assignment Problem (SAP, podproblem kwadratowego problemu przydziału Quadratic Assignment Problem -
QAP), czyli problemu przydziału miejsc (przy stołach) z uwzględnieniem ich rozmieszczenia, rozróżnienia stołów i wyboru miejsca centralnego.

# Opis algorytmu

Algorytm składa się z kilku części:

[![Algorytm genetyczny](https://i.imgur.com/TKVWnhp.png)](https://pl.wikipedia.org/wiki/Algorytm_genetyczny#Zapis_algorytmu)

  - Losowana jest pewna populacja początkowa złożona z genotypów (zakodowanych rozwiązań z dziedziny).
  - Populacja poddawana jest selekcji na podstawie jej oceny danej przez funkcję przystosowania (najlepiej przystosowane osobniki maja największe szanse na przetrwanie selekcji).
  - Genotypy są ze sobą krzyżowane.
  - Genotypy są poddawane mutacji (drobnym zmianom).
  - Tak powstała populacja wraca do kroku drugiego.


Zakodowane rozwiązanie (genotyp) definiujemy jako sekwencję indeksów osób do usadzenia (kodowanie permutacyjne), które stanowią bezpośrednie przyporządkowanie do kolejno występujących miejsc do rozdzielenia. Przykładowo dla osób **1**, **2** i **3** oraz miejsc **A**, **B**, **C**, genotyp **312** tłumaczy sie na fenotyp: osoba **3** do miejsca **A**, **1** do miejsca **B**, **2** do miejsca **C**.

Funkcję przystosowania (celu), którą należy maksymalizować (poziom zadowolenia) definiuje się jako:

![Funkcja przystosowania](https://i.imgur.com/JGfXjJv.png)

Natomiast jako operatory genetyczne wybrano:
  - **operator selekcji** - metoda koła ruletki. Koncepcyjne koło podzielone jest na wycinki o różnej szerokości wprost proporcjonalnej do wielkości wartości funkcji celu dla danego osobnika. Szerokości te najczęściej wyrażane są procentowo lub w ułamkach sumujących się do 1. Dokonując losowania liczby z przedziału [0; 1) wykonujemy strzał w dany zakres koła, dzięki czemu wybieramy osobnika, który kopiowany jest do nowej populacji. Operator polega na założeniu, że osobniki o korzystniejszej funkcji przystosowania mają większe szanse na przetrwanie. Nie eliminuje się jednak całkowicie istnienia osobników słabszych (zmniejsza się jedynie prawdopodobieństwo ich wyboru). 

  - **operator krzyżowania** - operator krzyżowania z porządkowaniem (Order Crossover - OX) z jednym punktem cięcia. Dla dwóch rodziców r 1 oraz r 2 losuje się punkt cięcia. Następnie tworzeni są potomkowie p 1 oraz p 2 , którzy dziedziczą część genów przed cięciem bezpośrednio od jednego rodzica, natomiast geny po cięciu układane sa zgodnie z kolejnością występowania tych genów u drugiego rodzica (zaczynając od części po cięciu, a po osiągnięciu końca łańcucha fragment przed cięciem).

  - **operator mutacji** - ze względu na zastosowanie reprezentacji permutacyjnej w części implementacyjnej należy zdefiniować jako zamianę ze sobą genów na dwóch losowo wybranych miejscach w chromosomie.

# Opis danych wejściowych

Dane wejściowe powinny być przygotowane w pliku konfiguracyjnym JSON; przykładowy plik poniżej:

```json
{
    "nodes": [
        {
            "id": 4,
            "priority": 4 
        },
        {
            "id": 3,
            "priority": 1
        },
        ...
    ],

    "edges": [
        {
            "id1": 4,
            "id2": 3,
            "weight": 3
        },
        ...
    ],

    "spots": [
        {
            "id": 0,
            "x": 0,
            "y": 1,
            "table_id": 1
        },
        {
            "id": 1,
            "x": 1,
            "y": 1,
            "table_id": 2
        },
        ...
    ],

    "parameters": {
        "central": {
            "x": 0,
            "y": 3
        },
        "alpha": 0.5,
        "beta": 0.5,
        "tablesdist": 3,
        "defweight": 1,
        "defpriority": 1,

        "mut_rate": 0.1,
        "cross_rate": 0.3,
        "iterations": 10,
        "population": 1000
    }
}
```

Plik konfiguracyjny składa się z kilku obowiązkowych części:

  - **definicja osób**

```json
{
    "nodes": [
        {
            "id": 4,
            "priority": 4 
        },
        {
            "id": 3,
            "priority": 1
        },
        ...
    ],
    ...
}
```

Definicja osób zawiera listę obiektów o identyfikatorze `id` (liczba całkowita bez znaku) oraz priorytecie `priority` (im wyższy priorytet, tym bliżej miejsca centralnego osoba powinna się znaleźć). Liczebność tej listy musi zgadzać się z liczebnością listy miejsc.


  - **definicja relacji**

```json
{
    "edges": [
        {
            "id1": 4,
            "id2": 3,
            "weight": 3
        },
        ...
    ],
    ...
}
```

Definicja relacji zawiera listę obiektów o identyfikatorach `id1` oraz `id2` (inicjator oraz adresat relacji) odpowiadających obiektom z listy osób oraz wadze `weight` (im wyższa tym bardziej pozytywny jest charakter relacji).

  - **definicja miejsc**

```json
{
    "spots": [
        {
            "id": 0,
            "x": 0,
            "y": 1,
            "table_id": 1
        },
        {
            "id": 1,
            "x": 1,
            "y": 1,
            "table_id": 2
        },
        ...
    ],
    ...
}
```

Definiacja miejsc zawiera listę obiektów o identyfikatorze `id` (liczba całkowita bez znaku), współrzędne `x` oraz `y` oraz identyfikator stolika `table_id` (liczba całkowita bez znaku - punkty przy różnych stolikach zostaną wirtualnie od siebie odsunięte). Liczebność tej listy musi zgadzać się z liczebnością listy osób.

  - **definicja parametrów**

```json
{
    "parameters": {
        "central": {
            "x": 0,
            "y": 3
        },
        "alpha": 0.5,
        "beta": 0.5,
        "tablesdist": 3,
        "defweight": 1,
        "defpriority": 1,

        "mut_rate": 0.1,
        "cross_rate": 0.3,
        "iterations": 10,
        "population": 1000
    },
    ...
}
```

Definicja parametrów dla funkcji celu (`alpha`, `beta`, `central` - współrzędne punktu centralnego, `tablesdist` - wirtualne rozsunięcie stolików), dla niekompletnych danych wejściowych (`defweight`, `defpriority` - parametry domyślne)  oraz algorytmu genetycznego (`mut_rate`/`cross_rate` - procent populacji, która podlefa mutacji/krzyżowaniu w danej iteracji, `iterations` - liczba iteracji, `population` - liczebność populacji)

# Opis rozwiązania

Szczegółowy opis działania, budowy i obsługi programu przedstawiony jest w podkatalogach [mpi/] oraz [upcxx/] odpowiednio dla każdego z rozwiązań.

[cmake]: <https://cmake.org/>
[fmt]: <https://github.com/fmtlib/fmt>
[json]: <https://nlohmann.github.io/json/>
[OpenMPI]: <https://www.open-mpi.org/>
[boost]: <https://www.boost.org/>
[boost_serialization]: <https://www.boost.org/doc/libs/1_67_0/libs/serialization/doc/index.html>
[boost_mpi]: <https://theboostcpplibraries.com/boost.mpi>

[input_20.json]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/config/input_20.json>
[input_50.json]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/config/input_50.json>
[config/]: <https://gitlab.com/czeslavo/parallel-settler/tree/master/config>
[mpi/]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/mpi/README.md>
[upcxx/]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/upcxx/README.md>