# **Równoległy algorytm genetyczny**  - Seating Assignment Problem *(temat 27)*

# Użyte narzędzia

Program został zaimplementowany przy użyciu bibliotek:

  - [OpenMPI],
  - [boost],
  - [boost_mpi], 
  - [boost_serialization], 
  - [json]
  - [fmt]. 

Do budowania użyto narzędzia [cmake].

# Opis budowy programu

Budowę programu można przeprowadzić przy użyciu narzędzia [cmake] (narzędzie dostępne np. na serwerze *Taurus*).

```sh
$ git clone git@gitlab.com:czeslavo/parallel-settler.git

$ cd mpi/

$ ./scripts/build.sh # this command will call cmake and make
```

# Opis działania programu

Szczegółowy opis działania programu przedstawia poniższy schemat.

![Schemat działania](https://i.imgur.com/rVkffCv.png)


# Opis obsługi programu

Po przeprowadzeniu kompilacji program można uruchomić za pomocą narzędzia `mpiexec` z wybraną liczbą węzłów, na których program będzie działał. Jako parametr należy podać plik konfiguracyjny, którego zawartość została omówiona w głównym [README]. Przykładowe pliki konfiguracyjne dla 10 ([input_10.json]), 20 ([input_20.json]) oraz 50 ([input_50.json]) osób znajdują się w katalogu [config/].

```sh
$ cd mpi/

$ cat nodes.txt
stud102
stud111
stud112

$ mpiexec -hostfile nodes.txt -n 4 build/main --params ../config/input_10.json
```

# Opis wyniku działania

Po uruchomieniu programu na standardowe wyjście zostanie wypisana informacja na temat najlepszego znalezionego wyniku w danej iteracji, jeżeli wynik został poprawiony oraz o numerze iteracji, w której to nastąpiło. Przykładowy output:

```sh
4burzynski@taurus:~/prir/project/mpi$ mpiexec -n 4 ./build/main --params ../config/input_20.json
! [Iteration 0] New global best: [14, 3, 13, 17, 10, 6, 8, 12, 11, 18, 20, 1, 2, 15, 16, 7, 4, 19, 9, 5] ->  31.2592
! [Iteration 367] New global best: [9, 13, 17, 11, 5, 15, 4, 7, 10, 12, 3, 14, 18, 2, 8, 6, 20, 16, 19, 1] ->  31.3997
```

# Poprawność wprowadzanych danych

Należy pamiętać, że by zrównoleglenie programu dało oczekiwane rezultaty (przyspieszenie czasu liczenia) trzeba dobrać odpowiednią wielkość populacji dla danej liczby węzłów (stosunek populacji do liczby węzłów nie powinien być zbyt mały, by czas łączenia danych nie przewyższał zysków ze zrównoleglenia obliczeń).


[cmake]: <https://cmake.org/>
[fmt]: <https://github.com/fmtlib/fmt>
[json]: <https://nlohmann.github.io/json/>
[OpenMPI]: <https://www.open-mpi.org/>
[boost]: <https://www.boost.org/>
[boost_serialization]: <https://www.boost.org/doc/libs/1_67_0/libs/serialization/doc/index.html>
[boost_mpi]: <https://theboostcpplibraries.com/boost.mpi>

[input_10.json]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/config/input_10.json>
[input_20.json]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/config/input_20.json>
[input_50.json]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/config/input_50.json>
[config/]: <https://gitlab.com/czeslavo/parallel-settler/tree/master/config>
[README]: <https://gitlab.com/czeslavo/parallel-settler/blob/master/README.md>