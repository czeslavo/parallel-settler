#pragma once

#include <vector>
#include <utility>
#include <ostream>

#include "boost/serialization/vector.hpp"
#include "boost/serialization/optional.hpp"
#include "boost/optional/optional_io.hpp"

#include "fmt/format.h"
#include "fmt/ostream.h"

namespace data_types
{

struct AlgoParams
{
    AlgoParams(
        double alpha,
        double beta,
        double mut_rate,
        double cross_rate,
        int iterations,
        int population
    ) 
        : alpha{alpha}, beta{beta}, mut_rate{mut_rate}, 
          cross_rate{cross_rate}, iterations{iterations},
          population{population}
    {}

    AlgoParams() = default;

    AlgoParams(AlgoParams&& o)
        : alpha{o.alpha}, beta{o.beta}, mut_rate{o.mut_rate}, 
          cross_rate{o.cross_rate}, iterations{o.iterations},
          population{o.population}
    {}

    AlgoParams& operator=(AlgoParams&& o) 
    {
        alpha = std::move(o.alpha);
        beta = std::move(o.beta);
        mut_rate = std::move(o.mut_rate);
        cross_rate = std::move(o.cross_rate);
        iterations = std::move(o.iterations);
        population = std::move(o.population);

        return *this;
    }

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & alpha;
        ar & beta;
        ar & mut_rate;
        ar & cross_rate;
        ar & iterations;
        ar & population;
    }

    int to_cross() const { return cross_rate * population; }
    int to_mutate() const { return mut_rate * population; }

    double alpha;
    double beta;
    double mut_rate;
    double cross_rate;
    int iterations;
    int population;
};

struct PersonVertex 
{
    PersonVertex(const int& id, int priority) 
        : id{id}, priority{priority} {}
   
    PersonVertex() = default;

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & id;
        ar & priority;
    }

    int id;
    int priority;
};

struct SpotVertex
{
    SpotVertex(int id, double center_dist, 
               double x, double y, int table_id) 
        : id{id}, center_dist{center_dist}, 
          x{x}, y{y}, 
          table_id{table_id} {}
    
    SpotVertex() = default;

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & id;
        ar & center_dist;
        ar & x;
        ar & y;
        ar & table_id;
    }

    int id;
    double center_dist;
    double x;
    double y;
    int table_id;
};

template <typename Vertex>
struct Edge
{
    Edge(const Vertex& v1, const Vertex& v2, double weight)
        : v1{v1}, v2{v2}, weight{weight} {}
   
    Edge() = default;

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & v1;
        ar & v2;
        ar & weight;
    }

    Vertex v1;
    Vertex v2;
    double weight;
};

template <typename Vertex>
struct Graph
{
    Graph(std::vector<Vertex>&& v, std::vector<Edge<Vertex>>&& e) 
        : vertices{v}, edges{e} {}
   
    Graph() = default;

    Graph(Graph&& o)
        : vertices{std::move(o.vertices)}, 
          edges{std::move(o.edges)}
    {}

    Graph& operator=(Graph&& o)
    {
        vertices = std::move(o.vertices);
        edges = std::move(o.edges);
    }

    double get_weight(int id1, int id2) const
    {
        const auto edge = std::find_if(
            edges.begin(), edges.end(),
            [id1, id2](const auto& e) 
            { 

                return e.v1.id == id1 and e.v2.id == id2 or
                       e.v2.id == id1 and e.v1.id == id2; 
            } 
        );
        
        if (edge == edges.end())
        {
            throw std::runtime_error(
                fmt::format("There's no edge for ids: {}-{}", id1, id2));
        }
        
        return edge->weight;
    }
    
    const auto& get_vertex(int id) const
    {
        const auto vertex_with_id = std::find_if(vertices.begin(), vertices.end(),
            [id](const auto& v) { return v.id == id; });
        
        if (vertex_with_id == vertices.end())
        {
            throw std::runtime_error(
               fmt::format("There's no vertex with id: {}", id));
        }

        return *vertex_with_id;
    }

    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & vertices;
        ar & edges;
    }

    std::vector<Vertex> vertices;
    std::vector<Edge<Vertex>> edges;
};


using RelationsGraph = Graph<PersonVertex>;
using SpotsGraph = Graph<SpotVertex>;

struct Genotype
{
    Genotype(std::vector<int>&& chromosomme)
        : chromosomme(std::move(chromosomme))
    {}

    Genotype() = default;
    Genotype(Genotype&& o) 
        : chromosomme{std::move(o.chromosomme)},
          fit{std::move(o.fit)}
    {}
    Genotype(const Genotype& o) 
        : chromosomme{o.chromosomme},
          fit{o.fit}
    {}

    Genotype& operator=(Genotype&& o)
    {
        chromosomme = std::move(o.chromosomme);
        fit = std::move(o.fit);
    }

    Genotype& operator=(const data_types::Genotype& o)
    {
        chromosomme = o.chromosomme;
        fit = o.fit;
    }
 
    template <typename Archive>
    void serialize(Archive& ar, const unsigned int version)
    {
        ar & chromosomme;
        ar & fit;
    }

    std::vector<int> chromosomme;
    boost::optional<double> fit;
};

inline std::ostream& operator<<(std::ostream& os, const Genotype& g)
{
    os << fmt::format("[{}] -> {}", fmt::join(g.chromosomme, ", "), g.fit);
    return os;
}

}
