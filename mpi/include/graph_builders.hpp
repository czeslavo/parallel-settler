#pragma once

#include <algorithm>
#include <boost/optional.hpp>

#include "json.hpp"
#include "data_types.hpp"

namespace builders
{
using json = nlohmann::json;

boost::optional<double> get_weight_for_ids(int id1, int id2, const json& input)
{
    const auto& edges = input["edges"];
    const auto found = std::find_if(edges.begin(), edges.end(), 
        [&id1, &id2](const auto& edge) { return edge["id1"] == id1 and edge["id2"] == id2; });

    if (found != edges.end()) 
    {
        return (*found)["weight"].get<double>();
    }
    
    return boost::none;
}

auto build_relations_graph(const json& input)
{
    using namespace data_types;
    
    std::vector<PersonVertex> vertices;
    
    for (const auto& node : input["nodes"])
    {
        vertices.emplace_back(node["id"], 
                              node["priority"]);
    }

    const double default_weight = input["parameters"]["defweight"];
    std::vector<Edge<PersonVertex>> edges; 
    for (int i = 0; i < vertices.size(); ++i)
    {
        for (int j = i + 1; j < vertices.size(); ++j)
        {
            const auto weight1 = get_weight_for_ids(vertices[i].id, vertices[j].id, input).value_or(default_weight);
            const auto weight2 = get_weight_for_ids(vertices[j].id, vertices[i].id, input).value_or(default_weight);

            const auto weight = (weight1 + weight2) / 2.;
            edges.emplace_back(vertices[i], vertices[j], weight);  
        }
    }

    return RelationsGraph{
        std::move(vertices), std::move(edges)
    };
}

double get_distance(double x1, double y1, double x2, double y2)
{
    return std::sqrt(std::pow(x1 - x2, 2) + std::pow(y1 - y2, 2));
}

double get_distance(const data_types::SpotVertex& s1, 
                    const data_types::SpotVertex& s2)
{
    return get_distance(s1.x, s1.y, s2.x, s2.y);
}

auto build_spots_graph(const json& input)
{
    using namespace data_types;
    
    const double central_x = input["parameters"]["central"]["x"];
    const double central_y = input["parameters"]["central"]["y"];
    
    std::vector<SpotVertex> vertices;
    
    for (const auto& spot : input["spots"])
    {
        const double x = spot["x"];
        const double y = spot["y"];
        const auto dist = get_distance(central_x, central_y, x, y);

        vertices.emplace_back(
            spot["id"],
            dist,
            x,
            y,
            spot["table_id"]
        );
    }

    std::vector<Edge<SpotVertex>> edges;
    for (int i = 0; i < vertices.size(); ++i)
    {
        for (int j = i + 1; j < vertices.size(); ++j)
        {
            const auto& v1 = vertices[i];
            const auto& v2 = vertices[j];

            edges.emplace_back(
                v1, v2, 
                get_distance(v1, v2) + 
                    (v1.table_id == v2.table_id ? 0. : input["parameters"]["tablesdist"].get<double>()) 
            );
        }
    }


    return SpotsGraph{
        std::move(vertices), std::move(edges)
    };
}

auto build_algo_params(const json& input)
{
    using namespace data_types;

    const auto& p = input["parameters"];
    return AlgoParams{
        p["alpha"],
        p["beta"],
        p["mut_rate"],
        p["cross_rate"],
        p["iterations"],
        p["population"]
    };
}

}
