#pragma once

#include <string>
#include <fstream>

#include "fmt/format.h"
#include "fmt/ostream.h"
#include "boost/program_options.hpp"
#include "json.hpp"

auto parse_command_args(int argc, char* argv[]) 
{
    struct Args 
    {
        std::string parameters_filename;    
    };

    namespace po = boost::program_options;

    po::options_description desc("Program arguments");
    desc.add_options()
        ("params", po::value<std::string>(), "json file with algorithm's parameters");

    po::variables_map vm;
    po::store(po::parse_command_line(argc, argv, desc), vm);
    po::notify(vm);

    if (vm.count("params") == 0)
    {
        fmt::print(stderr, "{}", desc);
        std::exit(1);
    }

    return Args{
        vm["params"].as<std::string>()
    };
}

auto get_algo_params(const std::string& filename)
{
    using json = nlohmann::json;

    std::ifstream input(filename);
    json params;
    input >> params;

    return params;
}
