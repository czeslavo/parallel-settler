#pragma once

#include <stdexcept>
#include <random>

#include "boost/range/counting_range.hpp"
#include "boost/mpi.hpp"
#include "fmt/format.h"

#include "data_types.hpp"
#include "utils.hpp"

template <typename World>
void join_population(World& world, 
                     const std::vector<data_types::Genotype>& population_part,
                     std::vector<data_types::Genotype>& whole_population)
{
    if (world.rank() == 0)
    {
        std::vector<std::vector<data_types::Genotype>> population_parts;
        gather(world, population_part, population_parts, 0);

        whole_population = merge(population_parts);
    }
    else 
    {
        gather(world, population_part, 0); 
    }
}

template <typename World>
auto get_equal_worker_part_of_population(World& world,
                                         const std::vector<data_types::Genotype>& population,
                                         int n)
{
    const std::size_t n_for_worker = n / world.size();
    std::vector<data_types::Genotype> worker_part{n_for_worker};
    scatter(world, population, worker_part.data(), n_for_worker, 0);
    
    return std::move(worker_part);
}


template <typename World>
void join_and_append_to_population(World& world,
                                   const std::vector<data_types::Genotype>& population_part,
                                   std::vector<data_types::Genotype>& whole_population)
{
    std::vector<data_types::Genotype> joined;
    join_population(world, population_part, joined);

    if (world.rank() == 0)
    {
        std::copy(joined.begin(),
                  joined.end(),
                  std::back_inserter(whole_population));
    }
}

template <typename World>
auto update_global_best(World& world, 
                        data_types::Genotype& global_best,  
                        const data_types::Genotype& local_best,
                        int iteration)
{
    auto get_best = [](const auto& lhs, const auto& rhs) { 
        return lhs.fit.get() > rhs.fit.get() ? 
            lhs : rhs; 
    };

    if (world.rank() == 0)
    {
        data_types::Genotype best; 
        reduce(world, local_best, best, get_best, 0);
        if (not global_best.fit or (best.fit.get() > global_best.fit.get()))
        {
            global_best = std::move(best);
            fmt::print("! [Iteration {}] New global best: {}\n", iteration, global_best);
        }
    }
    else
    {
        reduce(world, local_best, get_best, 0);
    }
}

auto get_local_best(const std::vector<data_types::Genotype>& population)
{
    const auto max_fit = std::max_element(
        population.begin(), 
        population.end(), 
        [](const auto& a, const auto& b) 
        { 
            return a.fit.get() < b.fit.get(); 
        }
    );

    return *max_fit;
}

double get_fit(const data_types::Genotype& genotype, 
               const data_types::RelationsGraph& rg,
               const data_types::SpotsGraph& sg,
               const data_types::AlgoParams& p)
{
    using namespace boost;

    double fit_p1 = 0.;

    const int size = genotype.chromosomme.size();
    for (const auto i : counting_range(0, size))
    {
        for (const auto j : counting_range(i + 1, size))
        {
            const auto id1 = genotype.chromosomme[i];
            const auto id2 = genotype.chromosomme[j];
            const auto relation_weight = rg.get_weight(id1, id2);
            
            const auto spot1 = sg.vertices[i].id;
            const auto spot2 = sg.vertices[j].id;
            const auto dist = sg.get_weight(spot1, spot2);
        
            fit_p1 += relation_weight / dist;
        }
    }
    
    double fit_p2 = 0;
    for (const auto i : counting_range(0, size))
    {
        const auto priority = rg.get_vertex(genotype.chromosomme[i]).priority;
        const auto dist_to_center = sg.get_vertex(sg.vertices[i].id).center_dist;
        fit_p2 += priority / dist_to_center;
    }

    return p.alpha * fit_p1 + p.beta * fit_p2;
}

void calculate_fit(std::vector<data_types::Genotype>& population,
                   const data_types::RelationsGraph& rg,
                   const data_types::SpotsGraph& sg,
                   const data_types::AlgoParams& p)
{
    for (auto& genotype : population)
    {
        if (not genotype.fit)
        {
            genotype.fit = get_fit(genotype, 
                                   rg,  
                                   sg,
                                   p);
        }
    }
    
}

template <typename World>
void designate_population_part(World& world,
                               std::vector<data_types::Genotype>& whole_population,
                               std::vector<data_types::Genotype>& designated_part,
                               int n,
                               std::random_device& rd)
{
    if (world.rank() == 0)
    {
        std::shuffle(whole_population.begin(), whole_population.end(), rd);
        
        designated_part.reserve(n);
        std::copy(whole_population.begin(), 
                  whole_population.begin() + n,
                  std::back_inserter(designated_part));

        whole_population.erase(whole_population.begin(),
                               whole_population.begin() + n);
    } 
}

