#pragma once

#include "data_types.hpp"
#include "boost/range/algorithm/random_shuffle.hpp"
#include "boost/range/algorithm/copy.hpp"
#include "boost/range/adaptors.hpp"
#include "boost/range/counting_range.hpp"

template <typename Generator>
auto shuffle_genotype(const std::vector<int>& genotype, Generator& g)
{
    std::vector<int> shuffled{genotype.begin(), genotype.end()};
    std::shuffle(shuffled.begin(), shuffled.end(), g);

    return std::move(shuffled); 
}

template <typename Generator>
auto get_initial_population(int population_size, 
                            const data_types::RelationsGraph& graph, 
                            Generator& g)
{
    using namespace boost::adaptors;
    using namespace boost;

    const auto id_of = [](const auto& v) { return v.id; };
    
    std::vector<int> ids;
    boost::copy(
        graph.vertices | transformed(id_of), 
        std::back_inserter(ids)
    );

    std::vector<data_types::Genotype> population;
    population.reserve(population_size);

    for (const auto& _ : counting_range(0, population_size))
    {
        population.emplace_back(shuffle_genotype(ids, g));
    }

    return population;
}

