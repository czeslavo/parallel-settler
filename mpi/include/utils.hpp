#pragma once

#include <vector>
#include <numeric>

template <typename T>
std::vector<T> merge(const std::vector<std::vector<T>>& vs)
{
    const auto size = std::accumulate(vs.begin(), vs.end(), 0, 
                                      [](const auto& acc, const auto& n) -> int
                                      {
                                          return acc + n.size();
                                      });
    std::vector<T> merged;
    merged.reserve(size);

    for (const auto& v : vs)
    {
        std::copy(v.begin(), v.end(), std::back_inserter(merged));
    }

    return std::move(merged);
}
