#!/bin/bash

# IMPORTANT!
# remember to add 
# using mpi ; 
# to project-config.jam 

./bootstrap.sh --with-libraries=filesystem,log,mpi,program_options,serialization,graph \
    --prefix=/home/stud2014/4burzynski/opt

./b2 install -j $(nproc)
