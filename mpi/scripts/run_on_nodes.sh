
# mpiexec must be for OpenMPI not MPICH! (/usr/bin/mpiexec probably)

mpiexec -hostfile nodes.txt -n 4 build/main --params

# or 
mpiexec -H host1,host2,host3 
