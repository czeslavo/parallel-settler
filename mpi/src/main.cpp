#include "parse_args.hpp"
#include "graph_builders.hpp"
#include "random.hpp"
#include "utils.hpp"
#include "population.hpp"
#include "selection.hpp"
#include "crossing.hpp"
#include "mutation.hpp"

#include <algorithm>
#include <random>
#include "boost/mpi.hpp"
#include "fmt/format.h"

namespace mpi = boost::mpi;

int main(int argc, char* argv[])
{
    mpi::environment env;
    mpi::communicator world;

    fmt::print("Hello! I'm processor {} with world id {}\n", env.processor_name(), 
                                                           world.rank());
    
    data_types::RelationsGraph relations_graph;
    data_types::SpotsGraph spots_graph;
    data_types::AlgoParams algo_params;

    // only main thread parses args
    if (world.rank() == 0)
    {
        const auto args = parse_command_args(argc, argv);
        const auto raw_params = get_algo_params(args.parameters_filename);

        relations_graph = builders::build_relations_graph(raw_params);
        spots_graph = builders::build_spots_graph(raw_params);
        algo_params = builders::build_algo_params(raw_params);
    }

    broadcast(world, relations_graph, 0); 
    broadcast(world, spots_graph, 0); 
    broadcast(world, algo_params, 0); 

    // generate part of population on each node
    std::random_device rd;
    auto population_part = get_initial_population(
        algo_params.population / world.size(), 
        relations_graph,
        rd
    );
    
    data_types::Genotype global_best;
    std::vector<data_types::Genotype> whole_population; // will be valid only for main thread
    
    for (const auto& iteration : boost::counting_range(0, algo_params.iterations))
    {
        calculate_fit(population_part, 
                      relations_graph,  
                      spots_graph,
                      algo_params);

        const auto local_best = get_local_best(population_part);
        update_global_best(world, global_best, local_best, iteration);
        
        // selection
        const auto selected_population = select_population(population_part,
                                                           rd);
        
        // join
        join_population(world, selected_population, whole_population);
        
        // crossing
        std::vector<data_types::Genotype> population_part_to_cross;
        designate_population_part(world,
                                  whole_population,
                                  population_part_to_cross,
                                  algo_params.to_cross(),
                                  rd);

        const auto worker_part_to_cross = get_equal_worker_part_of_population(
            world,
            population_part_to_cross,
            algo_params.to_cross());

        const auto crossed_worker_population = cross_population(worker_part_to_cross, rd); 

        // join and add 
        join_and_append_to_population(world, crossed_worker_population, whole_population);

        // mutation
        std::vector<data_types::Genotype> population_part_to_mutate;
        designate_population_part(world,
                                  whole_population,
                                  population_part_to_mutate,
                                  algo_params.to_mutate(),
                                  rd);

        const auto worker_part_to_mutate = get_equal_worker_part_of_population(
            world,
            population_part_to_mutate,
            algo_params.to_mutate());
        
        const auto mutated_worker_population = mutate_population(worker_part_to_mutate, rd);

        join_and_append_to_population(world, mutated_worker_population, whole_population);
        
        population_part = get_equal_worker_part_of_population(
            world,
            whole_population,
            algo_params.population);
    }
}
