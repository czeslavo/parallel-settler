#pragma once

#include <random>
#include "boost/range/irange.hpp"
#include "boost/range/adaptors.hpp"
#include "boost/range/iterator_range_core.hpp"
#include "boost/algorithm/cxx11/any_of.hpp"
#include "boost/range/join.hpp"

#include "data_types.hpp"
#include "population_upcxx.hpp"

auto cross_genotypes(const data_types::Genotype& g1, 
                     const data_types::Genotype& g2,
                     std::random_device& rd)
{
    const auto& ch1 = g1.chromosomme;
    const auto& ch2 = g2.chromosomme;
    const int size = g1.size;

    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(1, size - 2);

    const auto bend_point = dis(gen);

    std::vector<int> crossed_1{ch1.begin(), ch1.begin() + bend_point};
    std::vector<int> crossed_2{ch2.begin(), ch2.begin() + bend_point};

    for (const auto& n : boost::range::join(
                            boost::make_iterator_range(ch2.begin() + bend_point,
                                                       ch2.begin() + size),
                            boost::make_iterator_range(ch2.begin(),
                                                       ch2.begin() + bend_point)))
    {
        if (crossed_1.size() == size)
            break;

        if (not boost::algorithm::any_of_equal(crossed_1, n))
            crossed_1.emplace_back(n);
    }
    
    for (const auto& n : boost::range::join(
                            boost::make_iterator_range(ch1.begin() + bend_point,
                                                       ch1.begin() + size),
                            boost::make_iterator_range(ch1.begin(),
                                                       ch1.begin() + bend_point)))
    {
        if (crossed_2.size() == size)
            break;

        if (not boost::algorithm::any_of_equal(crossed_2, n))
            crossed_2.emplace_back(n);
    }
    
    return std::make_pair(crossed_1, crossed_2);
}

auto cross_population(auto population, 
                      std::random_device& rd)
{
    const int size = population.size();
    const int even_size = size % 2 == 0 ? size : size - 1;

    std::vector<data_types::Genotype> crossed;
    crossed.reserve(size);
    
    for (const auto& i : boost::irange(0, even_size, 2))
    {
        const int j = i + 1;

        auto p = cross_genotypes(population[i], population[j], rd);
        crossed.emplace_back(std::move(p.first));
        crossed.emplace_back(std::move(p.second));
    }

    if (even_size != size)
    {
        crossed.emplace_back(population[size-1]); 
    }

    return std::move(crossed);
}


