#pragma once

#include <random>
#include "boost/range/irange.hpp"
#include "boost/range/adaptors.hpp"
#include "boost/range/iterator_range_core.hpp"
#include "boost/algorithm/cxx11/any_of.hpp"
#include "boost/range/join.hpp"

#include "data_types.hpp"
#include "population_upcxx.hpp"

auto mutate_population(auto population, 
                      std::random_device& rd)
{
    std::vector<data_types::Genotype> mutated;
    mutated.reserve(population.size());
        
    const int chromosomme_size = population[0].size;
    std::mt19937 gen(rd());
    std::uniform_int_distribution<> dis(0, chromosomme_size - 1);
    
    for (const auto& g : population)
    {
        std::vector<int> v{g.chromosomme.begin(), g.chromosomme.begin() + chromosomme_size};
        const auto i = dis(gen);
        const auto j = dis(gen);
      
        std::swap(v[i], v[j]);

        mutated.emplace_back(std::move(v));
    }
    
    return std::move(mutated);
}
