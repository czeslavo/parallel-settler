#pragma once

#include "data_types.hpp"
#include "boost/range/counting_range.hpp"

double get_fit(const data_types::Genotype& genotype, 
               const data_types::RelationsGraph& rg,
               const data_types::SpotsGraph& sg,
               const data_types::AlgoParams& p)
{
    using namespace boost;

    double fit_p1 = 0.;

    const int size = genotype.size;
    for (const auto i : counting_range(0, size))
    {
        for (const auto j : counting_range(i + 1, size))
        {
            const auto id1 = genotype.chromosomme[i];
            const auto id2 = genotype.chromosomme[j];
            const auto relation_weight = rg.get_weight(id1, id2);
            
            const auto spot1 = sg.vertices[i].id;
            const auto spot2 = sg.vertices[j].id;
            const auto dist = sg.get_weight(spot1, spot2);
        
            fit_p1 += relation_weight / dist;
        }
    }
    
    double fit_p2 = 0;
    for (const auto i : counting_range(0, size))
    {
        const auto priority = rg.get_vertex(genotype.chromosomme[i]).priority;
        const auto dist_to_center = sg.get_vertex(sg.vertices[i].id).center_dist;
        fit_p2 += priority / dist_to_center;
    }

    return p.alpha * fit_p1 + p.beta * fit_p2;
}

void calculate_fit(std::vector<data_types::Genotype>& population,
				   const data_types::RelationsGraph& rg,
				   const data_types::SpotsGraph& sg,
				   const data_types::AlgoParams& p)
{
	for (auto& genotype : population)
	{
        genotype.fit = get_fit(genotype, 
                               rg,  
                               sg,
                               p);
	}	
}

auto get_local_best(const std::vector<data_types::Genotype>& population)
{
    const auto max_fit = std::max_element(
        population.begin(), 
        population.end(), 
        [](const auto& a, const auto& b) 
        { 
            return a.fit < b.fit; 
        }
    );

    return *max_fit;
}

extern std::vector<data_types::Genotype> whole_population;

void join_population_parts(std::vector<data_types::Genotype> part, bool should_clear=false)
{
    if (should_clear) whole_population.clear();
    
    upcxx::rpc(0, [](upcxx::view<data_types::Genotype> gs) {
        for (const auto& g : gs) whole_population.emplace_back(g);
    }, upcxx::make_view(part)).wait();
    upcxx::barrier();
}

extern data_types::Genotype global_best;

auto update_global_best(const data_types::Genotype& local_best,
                        int iteration)
{
    auto get_best = [](const auto& lhs, const auto& rhs) { 
        return lhs.fit > rhs.fit ? 
            lhs : rhs; 
    };

    static bool firstTime = true;

    if (firstTime) { 
        global_best = local_best;
        firstTime = false;
        return;
    }
    upcxx::barrier();
    auto best = upcxx::allreduce(local_best, get_best).wait();
    if (upcxx::rank_me() == 0 and best.fit > global_best.fit)
    {
        global_best = std::move(best);
        fmt::print("! [Iteration {}] New global best: {}\n", iteration, global_best);
    }
}

extern std::vector<data_types::Genotype> designated_population;

void designate_population_part(int n, std::random_device& rd)
{
    if (upcxx::rank_me() == 0) 
    {
        std::shuffle(whole_population.begin(), whole_population.end(), rd);
        
        designated_population.clear();
        designated_population.reserve(n);
        std::copy(whole_population.begin(), 
                  whole_population.begin() + n,
                  std::back_inserter(designated_population));

        whole_population.erase(whole_population.begin(),
                               whole_population.begin() + n);
    }
}

extern std::vector<data_types::Genotype> worker_population_part;

template <typename Fn>
void perform_and_join_back(Fn&& action)
{ 
    if (upcxx::rank_me() == 0) 
    {
        const auto worker_part = designated_population.size() / upcxx::rank_n();
        for (const auto worker : boost::counting_range(0, upcxx::rank_n()))
        {
            const auto start = worker * worker_part;
            const auto end = start + worker_part < designated_population.size() ? 
                             start + worker_part : designated_population.size();
            upcxx::rpc(worker, 
                       std::forward<Fn>(action), 
                       upcxx::make_view(designated_population.begin() + start,
                                        designated_population.begin() + end)).wait();
        }
    }
    
    upcxx::barrier();
    join_population_parts(worker_population_part, false);
}

void log_whole_population(const std::string& tag = "")
{
    if (upcxx::rank_me() == 0)
        fmt::print("[{}] Whole population size = {}\n", tag, whole_population.size());
}

void scatter_population_on_workers()
{
    if (upcxx::rank_me() == 0) 
    {
        const auto worker_part = whole_population.size() / upcxx::rank_n();
        for (const auto worker : boost::counting_range(0, upcxx::rank_n()))
        {
            const auto start = worker * worker_part;
            const auto end = start + worker_part < whole_population.size() ? 
                             start + worker_part : whole_population.size();
        
            upcxx::rpc(worker, [start, end](upcxx::view<data_types::Genotype> gs) {
                worker_population_part.clear();
                std::copy(gs.begin() + start, 
                          gs.begin() + end, 
                          std::back_inserter(worker_population_part));
            }, upcxx::make_view(whole_population)).wait();
        }
    }
    upcxx::barrier();
}
