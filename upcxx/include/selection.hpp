#pragma once

#include <random>
#include "boost/range/counting_range.hpp"
#include "boost/range/adaptors.hpp"

#include "data_types.hpp"

auto select_population(const std::vector<data_types::Genotype>& population, 
                       std::random_device& rd)
{
    using namespace boost::adaptors;
    
    const auto fit_sum = std::accumulate(population.begin(), 
                                         population.end(), 
                                         0.,
                                         [](auto& acc, const auto& b) { return acc + b.fit; });

    std::mt19937 gen{rd()}; 
    const int size = population.size();
    
    std::vector<double> steps;
    steps.reserve(size);

    // construct roulette 
    double current = 0.;
    for (const auto& g : population)
    {
        const auto g_range = g.fit / fit_sum;
        current += g_range;
        steps.emplace_back(current);
    }

    // pick random from roulette 
    std::vector<data_types::Genotype> selected;
    selected.reserve(size);
    for (const auto& _ : boost::counting_range(0, size))
    {
        const auto r = std::generate_canonical<double, 10>(gen);

        for (const auto s : steps | indexed(0))
        {
            if (r < s.value())
            {
                selected.emplace_back(population[s.index()]);
                break;
            }
        }
    }

    return std::move(selected);
}

