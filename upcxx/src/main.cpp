#include "upcxx/upcxx.hpp"
#include "fmt/format.h"
#include "boost/container/static_vector.hpp"

#include "parse_args.hpp"
#include "graph_builders.hpp"
#include "random.hpp"
#include "utils.hpp"
#include "population_upcxx.hpp"
#include "selection.hpp"
#include "mutation.hpp"
#include "crossing.hpp"

std::random_device rd;
std::vector<data_types::Genotype> whole_population;
std::vector<data_types::Genotype> worker_population_part;
std::vector<data_types::Genotype> designated_population;
data_types::Genotype global_best;

int main(int argc, char* argv[])
{
    upcxx::init();
    
    fmt::print("Hello: {}\n", upcxx::rank_me());

    const auto args = parse_command_args(argc, argv);
    const auto raw_params = get_algo_params(args.parameters_filename);
    
    data_types::AlgoParams algo_params = builders::build_algo_params(raw_params);
    data_types::RelationsGraph relations_graph = builders::build_relations_graph(raw_params);
    data_types::SpotsGraph spots_graph = builders::build_spots_graph(raw_params);
    
    worker_population_part = get_initial_population(
        algo_params.population / upcxx::rank_n(), 
        relations_graph,
        rd
    );

    for (const auto iteration : boost::counting_range(0, algo_params.iterations))
    {
        // evaluation
        calculate_fit(worker_population_part, 
                      relations_graph,  
                      spots_graph,
                      algo_params); 

        const auto local_best = get_local_best(worker_population_part);
        update_global_best(local_best, iteration);

        // selection
        const auto selected_population = select_population(worker_population_part,
                                                           rd);
        join_population_parts(selected_population, true);
        
        // mutation
        designate_population_part(algo_params.to_mutate(), rd);
        perform_and_join_back([](upcxx::view<data_types::Genotype> gs) {
            worker_population_part.clear();
            for (const auto& g : mutate_population(gs, rd)) 
                worker_population_part.emplace_back(g);
        });
        
        // crossing
        designate_population_part(algo_params.to_cross(), rd);
        perform_and_join_back([](upcxx::view<data_types::Genotype> gs) {
            worker_population_part.clear();
            for (const auto& g : cross_population(gs, rd)) 
                worker_population_part.emplace_back(g);
        });

        scatter_population_on_workers();
    }

    upcxx::finalize();
}
